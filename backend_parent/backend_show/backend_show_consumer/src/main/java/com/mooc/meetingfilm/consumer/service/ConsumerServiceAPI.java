package com.mooc.meetingfilm.consumer.service;

/**
 * @author : wangyoulang
 * @program : com.mooc.meetingfilm.consumer.service
 * @description :
 **/
public interface ConsumerServiceAPI {

    String sayHello(String message);

}
