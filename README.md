# SpringCloud_MeetingFilm
请支持正版课程，慕课网实战课程《深度解锁SpringCloud主流组件一战解决微服务诸多难题》

#### 项目简介
1. 微服务注册中心（Eureka）
    - 基础
        - Eureka Server构建使用
        - Eureka Client构建使用
        - Provier和Consumer调用演示
    - 进阶
        - 服务注册流程讲解
        - 服务续约流程讲解
        - 服务下线流程讲解
    - 面试
        - 多注册中心优劣势讲解
        - Eureka注册慢原因讲解
        - 自我保护模式讲解
2. 负载均衡器（Ribbon）
    - 基础
        - Ribbon架构图解析
        - Ribbon环境构建演示
        - Ribbon调用演示
    - 进阶
        - Ribbon参数及使用介绍
        - Ribbon负载均衡算法区别及演示
        - Ribbon的IPing算法区别及演示
    - 面试
        - Ribbon源码分析
        - Ribbon自定义负载均衡算法及实战场景讲解
        - Ribbon ServerList使用场景
3. 熔断降级利器（Hystrix）
    - 基础
        - Hystrix自我介绍
        - Hystrix架构图详解
        - Hystrix两种命令四种模式详解
    - 进阶
        - Hystrix隔离术详解和演示
        - Hystrix熔断机制详解和演示
        - Hystrix监控机制和使用
    - 面试
        - Hystrix如何合理设置线程池数量
        - Hystrix参数全解析
        - Hystrix各项参数调优
4. HTTPClient（Feign）
    - 基础
        - Feign入门介绍
        - Feign使用演示
        - Feign参数绑定介绍
    - 进阶
        - Feign核心之Client详解
        - Feign整合Ribbon实现负载均衡
        - Feign整合Hystrix实现降级
    - 面试
        - Feign自定义配置讲解演示
        - Feign调优核心之HttpClient
        - Feign调优核心之请求压缩优化
5. 微服务网关（Zuul/Gateway）
    - 基础
        - API网关介绍及其优缺点
        - Zuul/Gateway介绍及使用
        - Zuul/Gateway架构图解析
    - 进阶
        - Zuul/Gateway表达式详解
        - Filter全生成周期讲解演示
        - Zuul/Gateway整合Ribbon和Hystrix使用
    - 面试
        - Zuul/Gateway之Filter源码
        - Zuul/Gateway自定义Filter
        - Zuul/Gateway安全实战

#### 项目情况
《深度解锁SpringCloud主流组件一战解决微服务诸多难题》这门实战课程，因为电脑资源的原因，其中的【第13章 Docker入门到实践】、【第14章 安装部署】部分我并没有实现，所以这部分的一些代码，我并没有上传至gitee中，待日后完善这部风再上传这部分的代码。

#### 启动运行项目的先决条件
1. 运行我在materials文件夹->公共资料文件夹下放置的mooc_meetingfilm.sql文件，生成数据库

#### 补充提示
- 在项目中运到的问题记录及解决方法都已记于我的博客中：https://blog.csdn.net/weixin_43391312

