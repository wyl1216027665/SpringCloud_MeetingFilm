<properties>
  <java.version>1.8</java.version>
  <mybatis.plus.version>3.2.0</mybatis.plus.version>
  <druid.version>1.1.10</druid.version>
  <mysql.version>8.0.13</mysql.version>
  <beetl.version>2.9.10</beetl.version>
  <log4j.version>1.2.17</log4j.version>
</properties>

<dependencies>
  <!-- mybatis-plus依赖 -->
  <dependency>
    <groupId>com.baomidou</groupId>
    <artifactId>mybatis-plus-boot-starter</artifactId>
    <version>${mybatis.plus.version}</version>
  </dependency>

  <!-- Druid引入 -->
  <dependency>
    <groupId>com.alibaba</groupId>
    <artifactId>druid-spring-boot-starter</artifactId>
    <version>${druid.version}</version>
  </dependency>

  <!-- 数据源驱动 -->
  <dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <version>${mysql.version}</version>
  </dependency>

  <!-- 代码生成器依赖 -->
  <dependency>
    <groupId>com.baomidou</groupId>
    <artifactId>mybatis-plus-generator</artifactId>
    <version>${mybatis.plus.version}</version>
  </dependency>

  <dependency>
    <groupId>com.ibeetl</groupId>
    <artifactId>beetl</artifactId>
    <version>${beetl.version}</version>
  </dependency>

  <!-- 日志框架依赖 -->
  <dependency>
    <groupId>log4j</groupId>
    <artifactId>log4j</artifactId>
    <version>${log4j.version}</version>
  </dependency>
</dependencies>



<!-- 资源引入 -->
<resources>
    <resource>
        <directory>src/main/resources</directory>
        <filtering>true</filtering>
    </resource>
    <resource>
        <directory>src/main/java</directory>
        <includes>
            <include>**/*.xml</include>
        </includes>
    </resource>
</resources>