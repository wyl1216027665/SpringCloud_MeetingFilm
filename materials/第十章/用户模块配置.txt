server:
  port: 8201

eureka:
  client:
    service-url:
      defaultZone: http://localhost:8761/eureka/

spring:
  application:
    name: user-service
    
logging:
  config: classpath:logback.xml